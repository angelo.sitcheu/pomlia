"""Top-level package for Production Oriented Machine Learning for Image Analysis."""

__author__ = """Angelo Jovin Yamachui Sitcheu (IAI)"""
__email__ = "angelo.sitcheu@kit.edu"
__version__ = "0.1.0"
