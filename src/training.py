from argparse import ArgumentParser
from pytorch_lightning.loggers import TensorBoardLogger
import torch._dynamo
from torchinfo import summary
import pytorch_lightning as pl
from torch.utils.data import DataLoader

from models.autoencoder import ResNet18Autoencoder
from utils.read_config import get_config_params
from utils.make_dataset import DATA_TRANSFORM
from utils.repro import ensure
from utils.make_dataset import getMedMNISTDataset_2D


parser = ArgumentParser()
parser.add_argument(
    "--model_log_dir",
    type=str,
    default="medmnist_ae",
)
parser.add_argument(
    "--num_workers",
    type=int,
    default=4,
)
args = parser.parse_args()

# set configuration
params = get_config_params()
SEED = params["simulation"]["seeds"][-1]
ACCELERATOR = params["simulation"]["device"][1]
EPOCHS = params["approaches"]["ae"]["epochs"]
logger = TensorBoardLogger("logs", name=args.model_log_dir)
ensure(seed=SEED)
print(
    "Used Configurations: Seed {0}, device {1}, and number of epochs {2}".format(
        SEED, ACCELERATOR, EPOCHS
    )
)
model = ResNet18Autoencoder()
print("Summary of the model")
summary(model)
checkpoint_callback = pl.callbacks.ModelCheckpoint(
    monitor="val_loss",
    mode="min",
    filename="best_model",
    save_top_k=1,
)
train_set = getMedMNISTDataset_2D(split="train", transform=DATA_TRANSFORM)
train_loader = DataLoader(
    train_set,
    batch_size=model.batch_size,
    shuffle=True,
    num_workers=args.num_workers,
    pin_memory=torch.cuda.is_available(),
)
val_set = getMedMNISTDataset_2D(split="val", transform=DATA_TRANSFORM)
val_loader = DataLoader(
    val_set,
    batch_size=model.batch_size,
    shuffle=False,
    num_workers=args.num_workers,
    pin_memory=torch.cuda.is_available(),
)
trainer = pl.Trainer(
    accelerator=ACCELERATOR,
    devices=1,
    max_epochs=EPOCHS,
    precision="bf16-mixed",
    benchmark=True,
    strategy="auto",
    callbacks=checkpoint_callback,
    logger=logger,
)
trainer.fit(model, train_loader, val_loader)
