from argparse import ArgumentParser
import torch._dynamo
from torchinfo import summary
import numpy as np
from torch.utils.data import DataLoader

from models.autoencoder import ResNet18Autoencoder
from utils.read_config import get_config_params
from utils.make_dataset import DATA_TRANSFORM
from utils.repro import ensure
from utils.make_dataset import getMedMNISTSpecificDataset
from utils.visualize import plot_distance_matrix

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "--model_path",
        type=str,
        default="/Users/iw0941/Documents/Projects/pomlia/src/logs/ae_experiments/version_0/checkpoints/best_model.ckpt",
    )
    parser.add_argument(
        "--num_workers",
        type=int,
        default=4,
    )
    args = parser.parse_args()

    # set configuration
    params = get_config_params()
    SEED = params["simulation"]["seeds"][-1]
    ACCELERATOR = params["simulation"]["device"][1]
    ensure(seed=SEED)
    print("Used Configurations: Seed {0}, and device {1}".format(SEED, ACCELERATOR))
    model = ResNet18Autoencoder()
    print("Summary of the model")
    summary(model)
    # Load the best checkpoint
    best_autoencoder = model.load_from_checkpoint(args.model_path)
    best_autoencoder.eval()

    full_representations, d1, d2 = [], [], []
    datasetNames = [
        "bloodmnist",
        "breastmnist",
        "chestmnist",
        "dermamnist",
        "octmnist",
        "organamnist",
        "organcmnist",
        "organsmnist",
        "pathmnist",
        "pneumoniamnist",
        "retinamnist",
        "tissuemnist",
    ]
    for i, datasetName in enumerate(datasetNames):
        test_set = getMedMNISTSpecificDataset(
            name=datasetName, split="test", new_label=i, transform=DATA_TRANSFORM
        )
        test_loader = DataLoader(
            test_set,
            batch_size=model.batch_size,
            shuffle=False,
            num_workers=args.num_workers,
            pin_memory=torch.cuda.is_available(),
        )
        with torch.no_grad():
            for i, (batch, y, new_l) in enumerate(test_loader):
                encoded_representation = best_autoencoder.encoder(batch).cpu().numpy()
                new_l = new_l.cpu().numpy()[0]
                d1.append(new_l[:, 0][0])
                d2.append(new_l[:, 1][0])
        full_representations.append((np.mean(d1), np.mean(d2)))
        d1, d2 = [], []
    np.save("means.npy", np.array(full_representations))

    plot_distance_matrix(np.array(full_representations))

    """
    To plot some samples in a 2D Latent Space e.g. n = 10000 samples

    from utils.visualize import LEGEND_ELEMENTS, PLOT_COLOR_CODE
    fig, ax = plt.subplots(figsize=(1920 / dpi, 1080 / dpi))
    for i, datasetName in enumerate(datasetNames):
        test_set = getMedMNISTSpecificDataset(
            name=datasetName, split="test", new_label=i, transform=DATA_TRANSFORM
        )
        test_loader = DataLoader(
            test_set,
            batch_size=model.batch_size,
            shuffle=False,
            num_workers=args.num_workers,
            pin_memory=torch.cuda.is_available(),
        )
        with torch.no_grad():
            for i, (batch, y, new_l) in enumerate(test_loader):
                encoded_representation = best_autoencoder.encoder(batch).cpu().numpy()
                new_l = new_l.cpu().numpy()[0]
                x_axis = encoded_representation[:, 0]
                y_axis = encoded_representation[:, 1]
                ax.scatter(x_axis, y_axis, label=PLOT_COLOR_CODE[new_l][0], c=PLOT_COLOR_CODE[new_l][1],
                           marker=PLOT_COLOR_CODE[new_l][2])
                if i > 10000:
                    plt.subplots_adjust(right=0.60)
                    ax.set_xlabel('Dimension 1', fontsize=20)
                    ax.set_ylabel('Dimension 2', fontsize=20)
                    ax.tick_params(axis='both', labelsize=20)
                    ax.legend(handles=LEGEND_ELEMENTS, bbox_to_anchor=(1.05, 1), loc='upper left', fontsize=20)
                    ax.grid(True)
                    plt.savefig("MedMNIST_latent_space_test.png")
                    plt.show()
                    plt.clf()
                    break

    """
