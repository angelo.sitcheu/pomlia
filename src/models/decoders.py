import torch.nn as nn


class DecoderBasicBlock(nn.Module):
    """
    Decoder Basic Block
    """

    def __init__(self, input_channels: int, output_channels: int, stride: int = 1):
        super(DecoderBasicBlock, self).__init__()

        self.first_deconv_layer = nn.ConvTranspose2d(
            input_channels,
            output_channels,
            kernel_size=3,
            stride=stride,
            padding=1,
            output_padding=1,
            bias=False,
        )
        self.first_batch_normalization = nn.BatchNorm2d(output_channels)
        self.activation = nn.ReLU(inplace=True)
        self.second_deconv_layer = nn.ConvTranspose2d(
            output_channels,
            output_channels,
            kernel_size=3,
            stride=1,
            padding=1,
            bias=False,
        )
        self.second_batch_normalization = nn.BatchNorm2d(output_channels)

    def forward(self, x):
        output = self.activation(
            self.first_batch_normalization(self.first_deconv_layer(x))
        )
        output = self.activation(
            self.second_batch_normalization(self.second_deconv_layer(output))
        )
        return output


class Resnet18Decoder(nn.Module):
    """
    Building the ResNet 18 auto decoder
    """

    def __init__(
        self,
        number_layers: list = None,
        embedding_dim: int = 2,
        image_output_channel: int = 1,
    ):
        super(Resnet18Decoder, self).__init__()
        if number_layers is None:
            number_layers = [2, 2, 2, 2]
        self.current_out_channels = 512
        self.sigmoid_activation = nn.Sigmoid()
        self.linear_layer = nn.Linear(embedding_dim, self.current_out_channels)
        self.deconv_layer = nn.ConvTranspose2d(
            self.current_out_channels,
            256,
            kernel_size=3,
            stride=1,
            padding=0,
            output_padding=0,
            bias=False,
        )
        self.batch_normalization = nn.BatchNorm2d(self.current_out_channels)
        self.first_deconv_layer = self._make_layer(
            DecoderBasicBlock, 256, number_layers[3], stride=2
        )
        self.second_deconv_layer = self._make_layer(
            DecoderBasicBlock, 128, number_layers[2], stride=2
        )
        self.third_deconv_layer = self._make_layer(
            DecoderBasicBlock, 64, number_layers[1], stride=2
        )
        self.fourth_deconv_layer = self._make_layer(
            DecoderBasicBlock, 64, number_layers[0], stride=2
        )
        self.conv_out = nn.ConvTranspose2d(
            64,
            image_output_channel,
            kernel_size=3,
            stride=2,
            padding=1,
            output_padding=1,
            bias=False,
        )

    def _make_layer(self, Decoder, in_channels, num_blocks, stride):
        inner_layers = []
        for _ in range(1, num_blocks):
            inner_layers.append(Decoder(self.current_out_channels, in_channels, stride))
        self.current_out_channels = in_channels
        return nn.Sequential(*inner_layers)

    def forward(self, z):
        x = self.linear_layer(z)
        x = x.view(x.size(0), 512, 1, 1)
        x = self.first_deconv_layer(x)
        x = self.second_deconv_layer(x)
        x = self.third_deconv_layer(x)
        x = self.fourth_deconv_layer(x)
        x = self.conv_out(x)
        return self.sigmoid_activation(x)
