import torch.nn as nn
import torch.utils
import torch.distributions
import torch.optim as optim
from .encoders import Resnet18Encoder
from .decoders import Resnet18Decoder
import pytorch_lightning as pl

import os
import sys

current_directory = os.getcwd()
parent_directory = os.path.dirname(current_directory)
sys.path.append(os.path.join(parent_directory, "utils"))

from utils.read_config import get_config_params

# set configuration
params = get_config_params()
LOSS = params["approaches"]["ae"]["loss"]
OPTIMIZER = params["approaches"]["ae"]["optimizer"]
BATCH_SIZE = params["approaches"]["ae"]["batch_size"]


class ResNet18Autoencoder(pl.LightningModule):
    def __init__(self, embedding_dim: int = 2, num_in_channels: int = 3):
        super().__init__()
        self.encoder = Resnet18Encoder(
            embedding_dim=embedding_dim, image_input_channel=num_in_channels
        )
        self.decoder = Resnet18Decoder(
            embedding_dim=embedding_dim, image_output_channel=num_in_channels
        )
        self.loss = getattr(nn, LOSS)()
        self.optimizer = getattr(optim, OPTIMIZER)
        self.batch_size = BATCH_SIZE

    def forward(self, x):
        z = self.encoder(x)
        z = self.decoder(z)
        return z

    def training_step(self, batch, batch_idx):
        inputs, _, v_n = batch
        outputs = self(inputs)
        loss = self.loss(outputs, inputs)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        inputs, _, v_n = batch
        outputs = self(inputs)
        loss = self.loss(outputs, inputs)
        self.log("val_loss", loss, batch_size=BATCH_SIZE, sync_dist=True)

    def configure_optimizers(self):
        optimizer = self.optimizer(self.parameters(), lr=0.001)
        scheduler = {
            "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(
                optimizer, mode="min", factor=0.5, patience=3, min_lr=1e-6
            ),
            "monitor": "val_loss",
            "interval": "epoch",
            "frequency": 1,
        }
        return [optimizer], [scheduler]
