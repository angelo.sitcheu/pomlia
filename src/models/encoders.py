import torch.nn as nn


class EncoderBasicBlock(nn.Module):
    """
    Encoder Basic Block as in https://arxiv.org/abs/1512.03385
    """

    def __init__(self, input_channels: int, output_channels: int, stride: int = 1):
        super(EncoderBasicBlock, self).__init__()

        self.first_conv_layer = nn.Conv2d(
            input_channels,
            output_channels,
            kernel_size=3,
            stride=stride,
            padding=1,
            bias=False,
        )
        self.first_batch_normalization = nn.BatchNorm2d(output_channels)
        self.activation = nn.ReLU(inplace=True)

        self.second_conv_layer = nn.Conv2d(
            output_channels,
            output_channels,
            kernel_size=3,
            stride=1,
            padding=1,
            bias=False,
        )
        self.second_batch_normalization = nn.BatchNorm2d(output_channels)

        if stride != 1 or input_channels != output_channels:
            self.skip_connection = nn.Sequential(
                nn.Conv2d(
                    input_channels,
                    output_channels,
                    kernel_size=1,
                    stride=stride,
                    bias=False,
                ),
                nn.BatchNorm2d(output_channels),
            )
        else:
            self.skip_connection = nn.Identity()

    def forward(self, x):
        output = self.activation(
            self.first_batch_normalization(self.first_conv_layer(x))
        )
        output = self.second_batch_normalization(self.second_conv_layer(output))
        output += self.skip_connection(x)
        output = self.activation(output)
        return output


class Resnet18Encoder(nn.Module):
    """
    Building the ResNet 18 auto encoder as in https://arxiv.org/abs/1512.03385
    """

    def __init__(
        self,
        number_layers: list = None,
        embedding_dim: int = 2,
        image_input_channel: int = 1,
    ):
        super(Resnet18Encoder, self).__init__()
        if number_layers is None:
            number_layers = [2, 2, 2, 2]
        self.current_in_channels = 64
        self.activation = nn.ReLU(inplace=True)
        self.max_pool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.conv_layer = nn.Conv2d(
            image_input_channel,
            self.current_in_channels,
            kernel_size=3,
            stride=2,
            padding=1,
            bias=False,
        )
        self.batch_normalization = nn.BatchNorm2d(self.current_in_channels)
        self.first_layer = self._make_layer(
            EncoderBasicBlock, self.current_in_channels, number_layers[0], stride=1
        )
        self.second_layer = self._make_layer(
            EncoderBasicBlock, 128, number_layers[1], stride=2
        )
        self.third_layer = self._make_layer(
            EncoderBasicBlock, 256, number_layers[2], stride=2
        )
        self.fourth_layer = self._make_layer(
            EncoderBasicBlock, 512, number_layers[3], stride=2
        )
        self.linear_layer = nn.Linear(512, embedding_dim)

    def _make_layer(self, Encoder, out_channels, num_blocks, stride):
        inner_layers = []
        inner_layers.append(Encoder(self.current_in_channels, out_channels, stride))
        self.current_in_channels = out_channels
        for _ in range(1, num_blocks):
            inner_layers.append(Encoder(out_channels, out_channels))
        return nn.Sequential(*inner_layers)

    def forward(self, x):
        x = self.activation(self.batch_normalization(self.conv_layer(x)))
        x = self.max_pool(x)
        x = self.first_layer(x)
        x = self.second_layer(x)
        x = self.third_layer(x)
        x = self.fourth_layer(x)
        x = x.view(x.size(0), -1)
        z = self.linear_layer(x)
        return z
