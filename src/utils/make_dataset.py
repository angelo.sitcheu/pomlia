import numpy as np
from torch.utils.data import Dataset, ConcatDataset
from torchvision import transforms
from medmnist import INFO
import medmnist

DATA_TRANSFORM = transforms.Compose(
    [transforms.Resize((32, 32)), transforms.ToTensor()]
)


class CustomMNISTMED(Dataset):
    """
    Dataset construction fot the evaluation of experiments
    """

    def __init__(self, name: str, new_label: int, transform, split: str):
        self.transform = None
        self.new_label = new_label
        self.num_channel = INFO[name]["n_channels"]

        if self.num_channel == 1:
            temp_transform = transforms.Compose(
                [transforms.Grayscale(num_output_channels=3)]
            )
            self.transform = transforms.Compose([temp_transform, transform])
        elif self.num_channel == 3:
            self.transform = transform
        else:
            raise ValueError("Number of channels invalid")
        DataClass = getattr(medmnist, INFO[name]["python_class"])
        self.dataset = DataClass(split=split, transform=self.transform, download=True)

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        image, label = self.dataset[idx]
        # Disabling labels from the ChestMNIST dataset: It is an array of size 14 and thus causes a bug in the code
        if len(label) > 1:
            return image, np.array([2023], dtype=int), self.new_label
        return image, label, self.new_label


def getMedMNISTDataset_2D(split: str = "train", transform=None) -> ConcatDataset:
    """
    Gets all 2D datasets from the MedMNIST benchmark dataset. All images are 3 channel images

    :param split: train, val or test
    :param transform: transform function to preprocess images
    """

    datasetNames = [
        "bloodmnist",
        "breastmnist",
        "chestmnist",
        "dermamnist",
        "octmnist",
        "organamnist",
        "organcmnist",
        "organsmnist",
        "pathmnist",
        "pneumoniamnist",
        "retinamnist",
        "tissuemnist",
    ]
    datasets = []
    if split.lower() != "train" and split.lower() != "val" and split.lower() != "test":
        raise ValueError(
            "Please enter the split of the data you need. e.g train, test or val"
        )
    else:
        for i, datasetName in enumerate(datasetNames):
            datasets.append(
                CustomMNISTMED(
                    name=datasetName, transform=transform, new_label=i, split=split
                )
            )
        return ConcatDataset(datasets)


def getMedMNISTSpecificDataset(
    new_label: int, name: str, split: str = "train", transform=None
) -> Dataset:
    """
    Gets a specific dataset from the MedMNIST benchmark dataset.

    :param name: name of the dataset
    :param new_label: new label for the whole dataset
    :param split: train, val or test
    :param transform: transform function to preprocess images
    """

    if split.lower() != "train" and split.lower() != "val" and split.lower() != "test":
        raise ValueError(
            "Please enter the split of the data you need. e.g train, test or val"
        )
    else:
        return CustomMNISTMED(
            name=name, transform=transform, new_label=new_label, split=split
        )
