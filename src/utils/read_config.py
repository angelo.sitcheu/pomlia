import os
import yaml


def get_config_params(file_name: str = "config.yaml"):
    config = None
    current_directory = os.getcwd()
    parent_directory = os.path.dirname(current_directory)
    file_path = os.path.join(parent_directory, file_name)
    with open(file_path, "r") as yaml_file:
        config = yaml.safe_load(yaml_file)

    return config
