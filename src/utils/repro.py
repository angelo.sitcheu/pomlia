import os
import numpy as np
import torch
import random
import pytorch_lightning as pl


def ensure(seed: int):
    """
    Ensure reproducibility of the experiments by setting seed

    :param seed: Seed to set
    """
    # Attempt to ensure reproducibility
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed=seed)
    torch.manual_seed(seed=seed)
    random.seed(seed)
    pl.seed_everything(seed=seed)
