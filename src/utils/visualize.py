import numpy as np
import torch._dynamo
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


LABEL, COLOR, MARKER = None, None, None
PLOT_COLOR_CODE = {
    0: ("BloodMNIST", "blue", "X"),
    1: ("BreastMNIST", "green", "D"),
    2: ("ChestMNIST", "red", "P"),
    3: ("DermaMNIST", "cyan", "<"),
    4: ("OctMNIST", "magenta", ">"),
    5: ("OrganaMNIST", "black", "o"),
    6: ("OrgancMNIST", "orange", "o"),
    7: ("OrgansMNIST", "purple", "o"),
    8: ("PathMNIST", "pink", "^"),
    9: ("PneumoniaMNIST", "brown", "s"),
    10: ("RetinaMNIST", "gray", "v"),
    11: ("TissueMNIST", "olive", "*"),
}
LEGEND_ELEMENTS = [
    Line2D(
        [0],
        [0],
        marker=MARKER,
        color="w",
        label=LABEL,
        markerfacecolor=COLOR,
        markersize=8,
    )
    for _, (LABEL, COLOR, MARKER) in PLOT_COLOR_CODE.items()
]


def euclidean_distance_matrix(points):
    """
    Compute the Euclidean distance matrix between points.

    Parameters:
        points (numpy.ndarray): An array of shape (N, 2) where N is the number of points.

    Returns:
        numpy.ndarray: A square matrix of shape (N, N) where element (i, j) represents
                    the Euclidean distance between points[i] and points[j].
    """
    num_points = len(points)
    distances = np.zeros((num_points, num_points))

    for i in range(num_points):
        for j in range(i, num_points):
            if i == j:
                continue
            distance = np.linalg.norm(points[i] - points[j])
            distances[i, j] = distance
            distances[j, i] = distance  # Since the distance matrix is symmetric

    return distances


def plot_distance_matrix(means: np.ndarray):
    """
    Plot the distance between all datasets
    :param means: Array with mean embedded data for all datasets
    """

    distance_matrix = euclidean_distance_matrix(means)
    dpi = plt.gcf().get_dpi()
    first_elements = [item[0] for item in PLOT_COLOR_CODE.values()]
    print(first_elements)

    # Set the figure size to maximize display size
    fig, ax = plt.subplots(figsize=(1940 / dpi, 2000 / dpi))
    im = ax.imshow(distance_matrix, cmap="viridis", origin="upper")
    ax.set_xticks(np.arange(len(means)))
    ax.set_xticklabels(first_elements, fontsize=20, rotation=90)
    ax.set_yticks(np.arange(len(means)))
    ax.set_yticklabels(np.flipud(first_elements), fontsize=20, rotation=30)
    for i in range(len(first_elements)):
        for j in range(len(first_elements)):
            ax.text(
                j,
                i,
                f"{distance_matrix[i, j]:.2f}",
                ha="center",
                va="center",
                color="white",
                fontsize=14,
            )
    plt.subplots_adjust(top=1, right=0.99)
    plt.savefig("mean_distance_matrix.png")
    plt.clf()
