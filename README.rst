POMLIA: Production Oriented Machine Learning for Image Analysis
======================================


.. image:: docs/method.png
 :alt: Block diagram of pomlia

==================================================================================================================

POMLIA is an innovative holistic approach to enhance biomedical image analysis. The method includes a fingerprinting process that enables selecting the best models, datasets, and
model development strategy relative to the image analysis task at hand; an automated model development stage; and a continuous deployment and monitoring
process to ensure continuous learning. In this repository, a proof of concept for fingerprinting in microscopic image datasets is performed according to `this paper <https://arxiv.org/abs/2309.15521>`_ .

==================================================================================================================


Requirements
~~~~~~~~

* Hardware with Python installed (preferably a linux OS).
* Recommended: CUDA capable GPU

==================================================================================================================

Getting started
~~~~~~~~
1. Clone the 'POMLIA' repo from gitlab::

    git clone git@gitlab.kit.edu:angelo.sitcheu/pomlia.git
    cd pomlia

2. Setup the environment and activate it (please also feel free to use `anaconda <https://www.anaconda.com/>`_)::

    python -m venv pomlia_env
    source pomlia_env/bin/activate
    python -m ensurepip --upgrade
    pip install -r requirements.txt


3. Reproduce latent space by training. Navigate to the source directory and launch the training script with the arguments ``--num_workers`` to specify the number of workers and ``--model_log_dir`` to specify the login directory for thr trained models. Learning curves can be visualized using `tensorboard <https://www.tensorflow.org/tensorboard/get_started>`_ ::

    $ cd src
    $ python training.py --num_workers=4 --model_log_dir="log_model"

4. Visualise the results. Processing the test data will generate various plots showing samples in a 2D latent space and distances between datasets. Navigate to the source directory and launch the testing script with the arguments ``--num_workers`` to specify the number of workers and ``--model_path`` to specify the path where the weights of the model you are looking forward to test are::

    $ python testing.py --num_workers=4 --model_path="best_model"



The distance between the mean of all test images of each dataset can be see in this figure.

.. image:: docs/dataset_distance.png
 :width: 800
 :alt: Distance between datasets


==================================================================================================================

Publication
~~~~~~~~
If you use or find the content of this repository interesting for you, please cite the following article::

   @inproceedings{sitcheu2023mlops,
      title={MLOps for Scarce Image Data: A Use Case in Microscopic Image Analysis},
      author={Yamachui Sitcheu, Angelo  and Friederich, Nils and Baeuerle, Simon and Neumann, Oliver and Reischl, Markus and Mikut, Ralf},
      booktitle={Proceedings-33. Workshop Computational Intelligence: Berlin, 23.-24. November 2023},
      volume={23},
      pages={169},
      year={2023},
      organization={KIT Scientific Publishing}
      }



==================================================================================================================

License
==================
This project is licensed under the MIT License - see the  LICENSE file for details.
